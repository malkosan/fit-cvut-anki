# BI-MA2

## Vynechaná témata
 - důkaz integrace sudých, lichých a periodických funkcí ([5:20](https://courses.fit.cvut.cz/BI-MA2/build/bi-ma2-04-vypocet-urciteho-integralu.pdf))
 - téma Numerická Integrace s výjimkou lichoběžníkového pravidla ([6](https://courses.fit.cvut.cz/BI-MA2/build/bi-ma2-05-numericka-integrace.pdf))
 - důkaz věty odhadu součtu ([7:30](https://courses.fit.cvut.cz/BI-MA2/build/bi-ma2-06-ciselne-rady.pdf))
